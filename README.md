# Модуль форм #
необходим модуль https://bitbucket.org/project-tm/project.core

* ajax загрузка файлов в форму
* создание форм

Отправляет форму, и генерирует все события (почт, crm)

```
#!php
if (Bitrix\Main\Loader::includeModule('project.core')) {
    Project\Core\Form::add($FORM_ID, array(
        'NAME' => 'Имя',
        'EMAIL' => 'E-mail',
        'COMMENT' => 'Комментарий'
    ));
}
```